# GitLab QA trainings

## Build team training on GitLab QA

A great introduction to the GitLab QA project, its internal structure, how it's
used, and how to start contributing to it. Check out the
[video](https://youtu.be/Ym159ATYN_g) and
[slides](https://docs.google.com/presentation/d/1-3YlYTIBzd2kSjGVYGPq1xqz4O7qtfABhoZRi_PcGRg/edit?usp=sharing).
